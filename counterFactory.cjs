function counterfactory(){
    let counter='100'

    if(isNaN(Number(counter))){
        counter=0
    }
    else{
        counter=Number(counter)
    }
    
    function increment(){
        return counter+=1

    }
    function decrement(){
        return counter-=1
    }
    return {increment,decrement}


}
module.exports=counterfactory




