function limitFunctionCallCount(cb,n){

    if((typeof cb =='function' ) &&  !(isNaN(Number(n)))){
        n=Number(n)
   
        return {limit:function (){

            if(n>0){
                n--
                return cb()
                
            }
            else{
                return null
            }

            }
        }
    }else{
        return "not a function or not a number";
    }


}
function cb(){
    return "hello"
}


module.exports={limitFunctionCallCount,cb}